#!/usr/bin/python3

#    Copyright © 2020 - 2021 IceGuye

#     This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published
#    by the Free Software Foundation, version 3 of the License.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#    General Public License for more details.

#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see
#    <http://www.gnu.org/licenses/>.

import sys
import pygame.display
import pygame.midi
import pygame.mixer
from os import listdir
from os.path import isfile, exists
import math
import threading
import time
from iceorgandict import stops, stops_sheet
import atexit
import json

def print_devices():
    for n in range(pygame.midi.get_count()):
        print (n,pygame.midi.get_device_info(n))

def play_chiff(organ):
    global chiff_cnt
    chiff_channel =chiff_chan_list[chiff_cnt]
    chiff_sound.set_volume(clip_avoid_factor)
    threading.Thread(target=chiff_playing, args=[chiff_channel, chiff_sound]).start()
    if chiff_cnt < len(chiff_chan_list) - 1:
        chiff_cnt += 1
    else:
        chiff_cnt = 0
        
def chiff_playing(chiff_channel, chiff_sound):
    chiff_len = chiff_sound.get_length()
    chiff_len = chiff_len - 0.1
    chiff_channel.set_volume(1.0)
    chiff_channel.play(chiff_sound, -1, 0, chiff_fadein)
    time.sleep(chiff_len)
    chiff_channel.set_volume(0.0)

def express_volume(express):
    if express == 0:
        dbfs = -999
    else:
        dbfs = 40 * math.log(express/127)
    result_volume = math.exp(dbfs/8.65618)
    return result_volume

def adjust_volume(express):
    organ_list = ["Great", "Swell", "Pedal"]
    set_volume = express_volume(express)
    for i in organ_list:
        for j in registration[i]:
            for k in keypress2[i]:
                channel = stops[i][j]["Notes"][k]["Channel"]
                channel.set_volume(set_volume)

def play_sound(organ, note):
    for i in registration[organ]:
        channel = stops[organ][i]["Notes"][note]["Channel"]
        sound = stops[organ][i]["Notes"][note]["Sound"]
        sound.set_volume(clip_avoid_factor)
        set_volume = express_volume(express)
        channel.set_volume(set_volume)
        channel.play(sound, -1, 0, inst_fadein)

def play_one_stop(organ, stop):
    for i in keypress2[organ]:
        channel = stops[organ][stop]["Notes"][i]["Channel"]
        sound = stops[organ][stop]["Notes"][i]["Sound"]
        sound.set_volume(clip_avoid_factor)
        set_volume = express_volume(express)
        channel.set_volume(set_volume)
        channel.play(sound, -1, 0, inst_fadein)
        
def fadeout(organ, channel, key, fotime):
    seg = 0.05
    svl = 1.0
    min_vl = 0 * seg
    while (svl > min_vl):
        if keypress[organ][key] != 0:
            channel.set_volume(1.0)
            break
        svl = svl - seg
        channel.set_volume(svl)
        time.sleep(fotime / ((1 - min_vl) / seg))

def stop_sound(organ, note):
    for i in registration[organ]:
        channel = stops[organ][i]["Notes"][note]["Channel"]
        sound = stops[organ][i]["Notes"][note]["Sound"]
        threading.Thread(target=fadeout, args=[organ, channel, note, float(inst_fadeout/1000)]).start()

def fadeout_one_stop(organ, channel, key, fotime):
    seg = 0.05
    svl = 1.0
    min_vl = 0 * seg
    while (svl > min_vl):
        svl = svl - seg
        channel.set_volume(svl)
        time.sleep(fotime / ((1 - min_vl) / seg))

def stop_one_stop(organ, stop):
    for i in keypress2[organ]:
        channel = stops[organ][stop]["Notes"][i]["Channel"]
        sound = stops[organ][stop]["Notes"][i]["Sound"]
        threading.Thread(target=fadeout_one_stop, args=[organ, channel, i, float(inst_fadeout/1000)]).start()

def change_reg_sounds(organ, old_reg, new_reg):
    for i in saved_registrations[organ][old_reg]:
        stop_one_stop(organ, i)
    for i in saved_registrations[organ][new_reg]:
        play_one_stop(organ, i)
    
def is_stop_out(stop):
    result = False
    for i in registration:
        if stop in registration[i]:
            result = True
            break
    return result

def blit_stop_imgs():
    xy = 130

    # Blit great organ:
    
    cnt = 0
    for i in stops["Great"]:
        pos_x = (cnt % 3) * xy
        pos_y = math.floor(cnt / 3) * xy
        if is_stop_out(i):
            wblit = window.blit(stop_imgs["Great"][i]["Out"], (1396+pos_x, 6+pos_y))
            stop_imgs["Great"][i].update({"Wblit": wblit})
        else:
            wblit = window.blit(stop_imgs["Great"][i]["In"], (1396+pos_x+10, 6+pos_y+10))
            stop_imgs["Great"][i].update({"Wblit": wblit})
        cnt += 1

    # Blit swell organ:

    swell_list = []
    for i in stops["Swell"]:
        swell_list.append(i)
    i = len(swell_list) - 1
    rev_swell = []
    while (i >= 0):
        stop_name = swell_list[i]
        rev_swell.append(stop_name)
        i = i - 1       
    cnt = 0
    for i in rev_swell:
        pos_x = (cnt % 3) * xy
        pos_y = math.floor(cnt / 3) * xy
        if is_stop_out(i):
            wblit = window.blit(stop_imgs["Swell"][i]["Out"], (19+pos_x, 6+pos_y))
            stop_imgs["Swell"][i].update({"Wblit": wblit})
        else:
            wblit = window.blit(stop_imgs["Swell"][i]["In"], (19+pos_x+10, 6+pos_y+10))
            stop_imgs["Swell"][i].update({"Wblit": wblit})
        cnt += 1

    # Blit pedal:
    pedal_list = []
    for i in stops["Pedal"]:
        pedal_list.append(i)
    i = len(pedal_list) - 1
    rev_pedal = []
    while (i >= 0):
        stop_name = pedal_list[i]
        rev_pedal.append(stop_name)
        i = i - 1
    cnt = 0
    for i in rev_pedal:
        pos_x = (cnt % 5) * xy
        pos_y = math.floor(cnt / 5) * xy
        start_y = 6 + xy * 4
        if is_stop_out(i):
            wblit = window.blit(stop_imgs["Pedal"][i]["Out"], (19+pos_x, start_y+pos_y))
            stop_imgs["Pedal"][i].update({"Wblit": wblit})
        else:
            wblit = window.blit(stop_imgs["Pedal"][i]["In"], (19+pos_x+10, start_y+pos_y+10))
            stop_imgs["Pedal"][i].update({"Wblit": wblit})
        cnt += 1

    # Blit couplers:
    cnt = 0
    for i in coupler_imgs:
        start_x = 1396
        start_y = 6 + 5 * xy
        if i == "swell-great":
            pos_x = xy * 0
            if "Swell" not in couplers_input[great_input]:
                wblit = window.blit(coupler_imgs[i]["In"], (start_x+pos_x+10, start_y+10))
                coupler_imgs[i].update({"Wblit": wblit})
            else:
                wblit = window.blit(coupler_imgs[i]["Out"], (start_x+pos_x, start_y))
                coupler_imgs[i].update({"Wblit": wblit})
        elif i == "swell-pedal":
            pos_x = xy * 1
            if "Swell" not in couplers_input[pedal_input]:
                wblit = window.blit(coupler_imgs[i]["In"], (start_x+pos_x+10, start_y+10))
                coupler_imgs[i].update({"Wblit": wblit})
            else:
                wblit = window.blit(coupler_imgs[i]["Out"], (start_x+pos_x, start_y))
                coupler_imgs[i].update({"Wblit": wblit})
        elif i == "great-pedal":
            pos_x = xy * 2
            if "Great" not in couplers_input[pedal_input]:
                wblit = window.blit(coupler_imgs[i]["In"], (start_x+pos_x+10, start_y+10))
                coupler_imgs[i].update({"Wblit": wblit})
            else:
                wblit = window.blit(coupler_imgs[i]["Out"], (start_x+pos_x, start_y))
                coupler_imgs[i].update({"Wblit": wblit})
        cnt = cnt + 1

    # Blit registration buttons
    xy = 60
    for i in reg_imgs:
        cnt = 0
        for j in reg_imgs[i]:
            start_x = 455
            end_x = 1340
            delta_x = end_x - start_x
            all_x = xy * 10
            total_gap = delta_x - all_x
            gap = total_gap / 10
            pos_x = (xy + gap) * cnt
            if i == "Swell":
                start_y = 191
            elif i == "Great":
                start_y = 368
            else:
                start_y = 368 + xy
            if cnt == active_reg_nums[i]:
                wblit = window.blit(j["In"], (start_x+pos_x+5, start_y+5))
                j.update({"Wblit": wblit})
            else:
                wblit = window.blit(j["Out"], (start_x+pos_x, start_y))
                j.update({"Wblit": wblit})

            cnt = cnt + 1   

def activate_reg(great, swell, pedal):
    registration["Great"] = saved_registrations["Great"][great]
    registration["Swell"] = saved_registrations["Swell"][swell]
    registration["Pedal"] = saved_registrations["Pedal"][pedal]
    active_reg_nums["Great"] = great
    active_reg_nums["Swell"] = swell
    active_reg_nums["Pedal"] = pedal
    

def save_registration():
    f = open("registrations.json", "w")
    f.write(json.dumps(saved_registrations, sort_keys=True, indent=4))
    f.close()
    f = open("active_reg_nums.json", "w")
    f.write(json.dumps(active_reg_nums, sort_keys=True, indent=4))
    f.close()

# Pygame, hardware devices, and other initial settings.

pygame.midi.init()
pygame.display.init()
pygame.display.set_caption('IceOrgan')

print_devices()
selected_device = input("Select Great (Lower) Manual: ")
selected_device = int(selected_device)
great_input = pygame.midi.Input(selected_device)
great_channel = 0

print_devices()
selected_device = input("Select Swell (Upper) Manual: ")
selected_device = int(selected_device)
swell_input = pygame.midi.Input(selected_device)
swell_channel = 0

print_devices()
selected_device = input("Select Pedal: ")
selected_device = int(selected_device)
pedal_input = pygame.midi.Input(selected_device)
pedal_channel = 0

input_list = [great_input, swell_input, pedal_input]
couplers_input = {input_list[0]: ["Great"],
                  input_list[1]: ["Swell"],
                  input_list[2]: ["Pedal"]}
channel = {"Great": great_channel,
           "Swell": swell_channel,
           "Pedal": pedal_channel}
express = 127
great_keypress2 = []
great_keypress = []
swell_keypress2 = []
swell_keypress = []
pedal_keypress2 = []
pedal_keypress = []
keypress2 = {"Great": great_keypress2,
             "Swell": swell_keypress2,
             "Pedal": pedal_keypress2}
keypress = {"Great": great_keypress,
            "Swell": swell_keypress,
            "Pedal": pedal_keypress}
for i in keypress:
    note = 0
    while note <= 127:
        keypress[i].append(0)
        note += 1

pygame.mixer.pre_init(96000, 32, 2, 2048)
pygame.mixer.init()
max_channel = 4800
pygame.mixer.set_num_channels(max_channel)

# Sounds loading.

chan_cnt = 0
for i in stops:
    organ = stops[i]
    for j in organ:
        stop = organ[j]
        note = 0
        while note <= 127:
            notes = stop["Notes"]
            soundpath = i+"/"+j+"/note-"+str(note)+".wav"
            print(soundpath)
            notes.append({
                "Channel": pygame.mixer.Channel(chan_cnt),
                "Sound": pygame.mixer.Sound(soundpath),
            })
            chan_cnt += 1
            note += 1

chiff_cnt = 0
chiff_chan_cnt = max_channel - 1 - 128
chiff_chan_list = []
chiff_sound = pygame.mixer.Sound("effectsamples/Chiff-use.wav")
while chiff_chan_cnt < max_channel:
    chiff_chan_list.append(pygame.mixer.Channel(chiff_chan_cnt))
    chiff_chan_cnt += 1
            
inst_fadein = int(0.038 * 1000)
inst_fadeout = int(0.333 * 1000)
noise_breath_fadein = int(0.120 * 1000)
noise_breath_fadeout = int(0.5 * 1000)
noise_brown_fadein = int(1.0 * 1000)
noise_brown_fadeout = int(5.0 * 1000)
chiff_fadein = int(0.06 * 1000)
chiff_fadeout = int(5.0 * 1000)
clip_avoid_factor = 0.4

# GUI building and loading.

window = pygame.display.set_mode((1800, 800))
wood_backboard_img = pygame.image.load("images/wood-backboard.png")
great_manual_img = pygame.image.load("images/keyboard-88.png")
great_manual_img = pygame.transform.scale(great_manual_img, (960, 115))
swell_manual_img = pygame.image.load("images/keyboard-88.png")
swell_manual_img = pygame.transform.scale(swell_manual_img, (960, 115))
window.blit(wood_backboard_img, (0, 0))
window.blit(great_manual_img, (445, 253))
window.blit(swell_manual_img, (445, 74))

stop_imgs = {"Great": {},
             "Swell": {},
             "Pedal": {}}
for i in stop_imgs:
    imgs_path = "images/stops/"+i+"/"
    for j in listdir(imgs_path):
        img_path = imgs_path+j
        if "-sha" in j:
            stop_name_start = 0
            stop_name_end = j.find("-sha.png")
            stop_name = j[stop_name_start:stop_name_end]
        else:
            stop_name_start = 0
            stop_name_end = j.find(".png")
            stop_name = j[stop_name_start:stop_name_end]
        stop_imgs[i].update({stop_name: {"In": None,
                                         "Out": None}})
    for j in listdir(imgs_path):
        img_path = imgs_path+j
        img_load = pygame.image.load(img_path)
        print(img_path)
        if "-sha" in j:
            stop_name_start = 0
            stop_name_end = j.find("-sha.png")
            stop_name = j[stop_name_start:stop_name_end]
            img_load = pygame.transform.scale(img_load, (130, 130))
            stop_imgs[i][stop_name]["Out"] = img_load
        else:
            stop_name_start = 0
            stop_name_end = j.find(".png")
            stop_name = j[stop_name_start:stop_name_end]
            img_load = pygame.transform.scale(img_load, (110, 110))
            stop_imgs[i][stop_name]["In"] = img_load

coupler_imgs = {}
imgs_path = "images/stops/couplers/"
for i in listdir(imgs_path):
    img_path = imgs_path + i
    if "-sha" not in i:
        name_end = i.find(".png")
        coupler_name = i[0:name_end]
    else:
        name_end = i.find("-sha.png")
        coupler_name = i[0:name_end]
    coupler_imgs.update({coupler_name: {"In": None,
                                        "Out": None}})
for i in listdir(imgs_path):
    img_path = imgs_path + i
    img_load = pygame.image.load(img_path)
    print(img_path)
    if "-sha" not in i:
        name_end = i.find(".png")
        coupler_name = i[0:name_end]
        img_load = pygame.transform.scale(img_load, (110, 110))
        coupler_imgs[coupler_name]["In"] = img_load
    else:
        name_end = i.find("-sha.png")
        coupler_name = i[0:name_end]
        img_load = pygame.transform.scale(img_load, (130, 130))
        coupler_imgs[coupler_name]["Out"] = img_load

keypress_imgs = {"Great": [None] * 128,
                 "Swell": [None] * 128,
                 "Pedal": [None] * 128}
i = 21
while i <= 108:
    img_path = "images/keys/key-"+str(i)+".png"
    keypress_imgs["Great"][i] = pygame.image.load(img_path)
    keypress_imgs["Great"][i] = pygame.transform.scale(keypress_imgs["Great"][i], (960, 115))
    keypress_imgs["Swell"][i] = pygame.image.load(img_path)
    keypress_imgs["Swell"][i] = pygame.transform.scale(keypress_imgs["Swell"][i], (960, 115))
    i = i + 1

if isfile("registrations.json"):
    load_file = open("registrations.json", "r")
    saved_registrations = json.loads(load_file.read())
    load_file.close()
else:
    saved_registrations = {"Great": [],
                           "Swell": [],
                           "Pedal": []}
    i = 0
    while i < 10:
        saved_registrations["Great"].append([])
        saved_registrations["Swell"].append([])
        saved_registrations["Pedal"].append([])
        i = i + 1
atexit.register(save_registration)

registration = {"Great": None,
                "Swell": None,
                "Pedal": None}

if isfile("active_reg_nums.json"):
    load_file = open("active_reg_nums.json", "r")
    active_reg_nums = json.loads(load_file.read())
    activate_reg(active_reg_nums["Great"],
                 active_reg_nums["Swell"],
                 active_reg_nums["Pedal"],)
else:
    active_reg_nums = {"Great": None,
                       "Swell": None,
                       "Pedal": None}
    activate_reg(1, 1, 1)
    
reg_imgs = {"Great": [],
            "Swell": [],
            "Pedal": []}
for i in reg_imgs:
    j = 0
    while j < 10:
        img_path = "images/registrations/reg-"+str(j)+".png"
        img_sha_path = "images/registrations/reg-"+str(j)+"-sha.png"
        img_load = pygame.image.load(img_path)
        img_load = pygame.transform.scale(img_load, (50, 50))
        img_sha_load = pygame.image.load(img_sha_path)
        img_sha_load = pygame.transform.scale(img_sha_load, (60, 60))
        img_dict = {"In": img_load,
                    "Out": img_sha_load}
        reg_imgs[i].append(img_dict)
        j = j + 1

blit_stop_imgs()

print("Loading completed, enjoy!")

while True:
    
    for evt in pygame.event.get():
        if evt.type == pygame.QUIT:
            pygame.quit()
            sys.exit()
        elif evt.type == pygame.MOUSEBUTTONDOWN:
            click_stop = None
            click_organ = None
            for i in stop_imgs:
                for j in stop_imgs[i]:
                    if "Wblit" in stop_imgs[i][j]:
                        if stop_imgs[i][j]["Wblit"].collidepoint(evt.pos):
                            click_stop = j
                            click_organ = i
                            break
                if click_stop is not None:
                    break
            if click_stop is not None:
                if click_stop in registration[click_organ]:
                    registration[click_organ].remove(click_stop)
                    stop_one_stop(click_organ, click_stop)
                else:
                    registration[click_organ].append(click_stop)
                    play_one_stop(click_organ, click_stop)
            for i in coupler_imgs:
                if coupler_imgs[i]["Wblit"].collidepoint(evt.pos):
                    if i == "swell-great":
                        if "Swell" not in couplers_input[great_input]:
                            couplers_input[great_input].append("Swell")
                        else:
                            couplers_input[great_input].remove("Swell")
                    elif i == "great-pedal":
                        if "Great" not in couplers_input[pedal_input]:
                            couplers_input[pedal_input].append("Great")
                        else:
                            couplers_input[pedal_input].remove("Great")
                    elif i == "swell-pedal":
                        if "Swell" not in couplers_input[pedal_input]:
                            couplers_input[pedal_input].append("Swell")
                        else:
                            couplers_input[pedal_input].remove("Swell")
            for i in reg_imgs:
                j = 0
                while j < len(reg_imgs[i]):
                    if reg_imgs[i][j]["Wblit"].collidepoint(evt.pos):
                        reg_num_tmp = {"Great": 999,
                                       "Swell": 999,
                                       "Pedal": 999}
                        for k in active_reg_nums:
                            if i == k:
                                reg_num_tmp[k] = j
                                old_reg = active_reg_nums[k]
                                new_reg = j
                                change_reg_sounds(k, old_reg, new_reg)
                            else:
                                reg_num_tmp[k] = active_reg_nums[k]
                        activate_reg(reg_num_tmp["Great"],
                                     reg_num_tmp["Swell"],
                                     reg_num_tmp["Pedal"],)
                    j = j + 1
                    
    window.blit(wood_backboard_img, (0, 0))
    window.blit(great_manual_img, (426, 253))
    window.blit(swell_manual_img, (426, 74))
    blit_stop_imgs()

    for i in input_list:
        coupler_list = couplers_input[i]
        if i.poll():
            input_read_all =  i.read(1)
            input_read = input_read_all[0][0]
            for j in coupler_list:
                if input_read[0] == 128 + channel[j]:
                    input_read[0] = 144 + channel[j]
                print(input_read)
                if input_read[0] == 176 + channel[j] and input_read[1] == 7:
                    if i == great_input:
                        express = input_read[2]
                        threading.Thread(target=adjust_volume,
                                         args=[express]).start()
                        #adjust_volume(express)
                if input_read[0] == 144 + channel[j] and input_read[2] == 0:
                    keypress[j][input_read[1]] = input_read[2]
                    for item in list(keypress2[j]):
                        if item == input_read[1]:
                            keypress2[j].remove(item)
                            stop_sound(j, input_read[1])
                elif input_read[0] == 144 + channel[j] and input_read[2] != 0:
                    keypress[j][input_read[1]] = input_read[2]
                    keypress2[j].append(input_read[1])
                    play_sound(j, input_read[1])
                    if len(registration[j]) > 0:
                        threading.Thread(target=play_chiff, args=[j]).start()

    for i in keypress2["Great"]:
        keypress_imgs["Great"][i].set_alpha(200)
        window.blit(keypress_imgs["Great"][i], (426, 253))
    for i in keypress2["Swell"]:
        keypress_imgs["Swell"][i].set_alpha(200)
        window.blit(keypress_imgs["Swell"][i], (426, 74))

    pygame.display.flip()
