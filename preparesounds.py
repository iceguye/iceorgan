#!/usr/bin/python3

#    Copyright © 2020 - 2021 IceGuye

#     This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published
#    by the Free Software Foundation, version 3 of the License.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#    General Public License for more details.

#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see
#    <http://www.gnu.org/licenses/>.

import os
import sys
import shutil
import math
from iceorgandict import stops, instrument_data

arg_ls = sys.argv
if (len(arg_ls) > 2):
    error = "You can only have one argument."
    print(error)
    sys.exit()
    
if len(arg_ls) == 2:
    ahz = float(arg_ls[1])
else:
    ahz = 440

hz_factor = ahz / ((2 ** (1/12)) ** 69)

inst = 1
while inst <= 9:
    inst_str = "Instrument" + str(inst)
    if os.path.exists(inst_str):
        shutil.rmtree(inst_str)
    os.mkdir(inst_str)
    note = 0
    while note <= 127:
        print("Generating sound files Instrument" + str(inst) + "/sine-" + str(note) + ".wav")
        atte = instrument_data[inst_str]["Attenuation (dB)"]
        db_adjust = -(atte/2.5) - 3
        volume = math.exp(db_adjust/8.65618)
        fs = 96000
        br = 24
        pan = instrument_data[inst_str]["Pan"][note+1]
        if pan <= 0:
            pan_l = str(1.0)
            pan_r =  str(1.0 - abs(pan/50))
        else:
            pan_l = str(1.0 - abs(pan/50))
            pan_r = str(1.0)
        tuning_semi_tone = instrument_data[inst_str]["Tuning (semi-tones)"]
        tuning_cents = instrument_data[inst_str]["Tuning (cents)"][note+1]
        if tuning_cents < 0:
            tuning = tuning_semi_tone - (tuning_cents / 100)
        else:
            tuning = tuning_semi_tone + (tuning_cents / 100)
        semi_tone = note + tuning
        tremolo_freq = instrument_data[inst_str]["Vib LFO freq(Hz)"]
        tremolo_pitch = abs(instrument_data[inst_str]["Vib LFO pitch(c)"])
        hertz = hz_factor * ((2 ** (1/12)) ** semi_tone)
        sec = 1.0
        os.system("sox -V -r " + str(fs) + " -n -b " + str(br) + " -c 1 Instrument" + str(inst) + "/sine-l-" + str(note) + ".wav synth " + str(sec) + " sin " + str(hertz) + " vol " + str(db_adjust) + "dB vol " + pan_l + " amplitude")
        os.system("sox -V -r " + str(fs) + " -n -b " + str(br) + " -c 1 Instrument" + str(inst) + "/sine-r-" + str(note) + ".wav synth " + str(sec) + " sin " + str(hertz) + " vol " + str(db_adjust) + "dB vol " + pan_r + " amplitude")
        if tremolo_pitch != 0:
            os.system("sox Instrument" + str(inst) + "/sine-l-" + str(note) + ".wav Instrument" + str(inst) + "/sine-r-" + str(note) + ".wav -c 2 --combine merge Instrument" + str(inst) + "/sine-f-" + str(note) + ".wav tremolo " + str(tremolo_freq) + " " + str(tremolo_pitch) + " reverb 70 fade t 0 -0 0.3")
            os.system("sox -v -1.0 Instrument" + str(inst) + "/sine-f-" + str(note) + ".wav Instrument" + str(inst) + "/sine-b-" + str(note) + ".wav reverse fade t 0.3 -0 0 pad " + str(sec - 0.6) + " 0")
            os.system("sox -m Instrument" + str(inst) + "/sine-f-" + str(note) + ".wav Instrument" + str(inst) + "/sine-b-" + str(note) + ".wav Instrument" + str(inst) + "/sine-" + str(note) + ".wav")
        else:
            os.system("sox Instrument" + str(inst) + "/sine-l-" + str(note) + ".wav Instrument" + str(inst) + "/sine-r-" + str(note) + ".wav -c 2 --combine merge Instrument" + str(inst) + "/sine-f-" + str(note) + ".wav reverb 70 fade t 0 -0 0.3")
            os.system("sox -v -1.0 Instrument" + str(inst) + "/sine-f-" + str(note) + ".wav Instrument" + str(inst) + "/sine-b-" + str(note) + ".wav reverse fade t 0.3 -0 0 pad " + str(sec - 0.6) + " 0")
            os.system("sox -m Instrument" + str(inst) + "/sine-f-" + str(note) + ".wav Instrument" + str(inst) + "/sine-b-" + str(note) + ".wav Instrument" + str(inst) + "/sine-" + str(note) + ".wav")
        os.remove("Instrument" + str(inst) + "/sine-l-" + str(note) + ".wav")
        os.remove("Instrument" + str(inst) + "/sine-r-" + str(note) + ".wav")
        os.remove("Instrument" + str(inst) + "/sine-f-" + str(note) + ".wav")
        os.remove("Instrument" + str(inst) + "/sine-b-" + str(note) + ".wav")
        note = note + 1
    inst = inst + 1

# Manipulate the effect samples

atte = 50 + 30
db_adjust = -(atte/2.5)
src = "effectsamples/noise_breath.wav"
dst = "effectsamples/noise_breath-use.wav"
start_dst = "effectsamples/noise_breath-start-use.wav"
loop_dst = "effectsamples/noise_breath-loop-use.wav"
print(start_dst)
print(loop_dst)
os.system("sox "+src+" -c 2 -r "+str(fs)+" -b "+str(br)+" "+dst+" vol "+str(db_adjust)+"dB pitch 100")
os.system("sox " + dst + " " + start_dst + " trim 0 1.43865216")
os.system("sox " + dst + " " + loop_dst + " trim 1.43865216 1.4")
os.remove(dst)

atte = 42 + 30
db_adjust = -(atte/2.5)
src = "effectsamples/Chiff.wav"
dst = "effectsamples/Chiff-use.wav"
print(dst)
os.system("sox  " + src + " -c 2 -r "+str(fs)+" -b "+str(br)+" " + dst + " vol " + str(db_adjust) + "dB fade t 0 -0 5.0")

atte = 12 + 30
db_adjust = -(atte/2.5)
scale_tuning = 25
pitch_inp = 100 * (scale_tuning/100)
src = "effectsamples/hall.wav"
os.system("sox " + src + " -r "+str(fs)+" -b "+str(br)+ " effectsamples/hall-l.wav vol " + str(db_adjust) + "dB pitch -1200")
os.system("sox " + src + " -r "+str(fs)+" -b "+str(br)+ " effectsamples/hall-r.wav vol " + str(db_adjust) + "dB pitch -600")

if os.path.exists("effectsamples/halls"):
    shutil.rmtree("effectsamples/halls")
os.mkdir("effectsamples/halls")
i = 0
while i <= 127:
    note_delta = i - 60
    pitch = note_delta * pitch_inp
    os.system("sox effectsamples/hall-l.wav effectsamples/hall-r.wav -c 2 --combine merge effectsamples/halls/hall-" + str(i) + ".wav pitch " + str(pitch))
    print("effectsamples/halls/hall-" + str(i) + ".wav")
    i = i + 1
os.remove("effectsamples/hall-l.wav")
os.remove("effectsamples/hall-r.wav")

for i in stops:
    if os.path.exists(i):
        shutil.rmtree(i)
    os.mkdir(i)
    for j in stops[i]:
        if os.path.exists(i+"/"+j):
            shutil.rmtree(i+"/"+j)
        os.mkdir(i+"/"+j)
        for k in stops[i][j]:
            if type(stops[i][j][k]) is int:
                print("Error: "+str(stops[i][j][k])+" is int.")
                exit()
            if type(stops[i][j][k]) is float:
                db = -(stops[i][j][k]/2.5)
                stops[i][j][k] = db
        note = 0
        while note <= 127:
            file_txt_list = ""
            fpath = '\"'+i+'/'+j+'\"'
            has_global = False
            global_db = 0.0
            for k in stops[i][j]:
                if k == "Global":
                    global_db = stops[i][j][k]    
            for k in stops[i][j]:
                value = stops[i][j][k]
                if "Instrument" in k and value != None:
                    db = -(stops[i][j][k] + global_db)
                    os.system("sox "+k+"/sine-"+str(note)+".wav "+fpath+"/"+k+".wav vol "+str(db)+"dB")
                    file_txt_list += fpath+"/"+k+".wav "
                elif "Noise breath" in k:
                    file_txt_list += "effectsamples/noise_breath-loop-use.wav "
            os.system("sox -m "+file_txt_list+fpath+"/note-"+str(note)+".wav")
            for k in stops[i][j]:
                value = stops[i][j][k]
                if "Instrument" in k and value != None:
                    os.remove(i+"/"+j+"/"+k+".wav")
            print(fpath+"/note-"+str(note)+".wav")
            note = note + 1

i = 1
while i <= 9:
    shutil.rmtree("Instrument"+str(i))
    i = i + 1

print("Sound preparation completed. Run iceorgan.py to play.")
