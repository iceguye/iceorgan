#!/usr/bin/python3

#    Copyright © 2020 - 2021 IceGuye

#     This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published
#    by the Free Software Foundation, version 3 of the License.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#    General Public License for more details.

#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see
#    <http://www.gnu.org/licenses/>.

def poly_to_list(raw_data):
    find_pos = raw_data.find("	")
    list_data = []
    while find_pos != -1:
        if raw_data[:find_pos] == "!":
            list_data.append(None)
        else:
            list_data.append(float(raw_data[:find_pos]))
        raw_data = raw_data[find_pos + len("	"):]
        find_pos = raw_data.find("	")
    if raw_data == "!":
        list_data.append(None)
    else:
        list_data.append(float(raw_data))
    return(list_data)

instrument_data = {
    "Instrument1": {
        "Attenuation (dB)": 18,
        "Pan": poly_to_list("!	6.7	21.0	-7.5	35.3	-21.8	49.6	-36.0	-6.7	7.5	-21.0	21.8	-35.3	36.0	-49.6	6.0	20.3	-8.2	34.5	-22.5	48.8	-36.8	-6.0	8.2	-20.3	22.5	-34.5	36.8	-48.8	5.2	19.5	-9.0	33.8	-23.3	48.1	-37.5	-5.2	9.0	-19.5	23.3	-33.8	37.5	-48.1	4.5	18.7	-9.7	33.0	-24.0	47.3	-38.3	-4.5	9.7	-18.7	24.0	-33.0	38.3	-47.3	3.7	18.0	-10.5	32.3	-24.8	46.6	-39.0	-3.7	10.5	-18.0	24.8	-32.3	39.0	-46.6	3.0	17.2	-11.2	31.5	-25.5	45.8	-39.8	-3.0	11.2	-17.2	25.5	-31.5	39.8	-45.8	2.2	16.5	-12.0	30.8	-26.3	45.1	-40.6	-2.2	12.0	-16.5	26.3	-30.8	40.6	-45.1	1.5	15.7	-12.7	30.0	-27.0	44.3	-41.3	-1.5	12.7	-15.7	27.0	-30.0	41.3	-44.3	0.7	15.0	-13.5	29.3	-27.8	43.6	-42.1	-0.7	13.5	-15.0	27.8	-29.3	42.1	-43.6	0.0	14.2"),
        "Tuning (semi-tones)": -12,
        "Tuning (cents)": poly_to_list("!	2	6	0	8	0	7	8	8	6	7	7	3	1	3	4	3	5	8	1	4	0	3	1	4	4	3	7	7	3	3	1	2	7	3	1	1	3	4	6	6	7	3	6	1	3	0	2	7	2	6	6	4	1	1	1	7	6	6	4	7	6	0	7	4	1	3	5	3	4	4	8	5	7	6	6	6	3	8	1	0	2	5	3	3	6	1	5	1	3	3	7	6	6	5	2	5	3	1	8	6	5	1	5	3	2	4	2	1	5	1	4	5	0	6	0	1	5	8	4	0	7	0	0	7	4	7	1	1"),
        "Mod LFO freq(Hz)": 6.871,
        "Mod LFO pitch(c)": 0,
        "Vib LFO freq(Hz)": 3.440,
        "Vib LFO pitch(c)": -4,
    },
    "Instrument2": {
        "Attenuation (dB)": 18,
        "Pan": poly_to_list("!	6.7	21.0	-7.5	35.3	-21.8	49.6	-36.0	-6.7	7.5	-21.0	21.8	-35.3	36.0	-49.6	6.0	20.3	-8.2	34.5	-22.5	48.8	-36.8	-6.0	8.2	-20.3	22.5	-34.5	36.8	-48.8	5.2	19.5	-9.0	33.8	-23.3	48.1	-37.5	-5.2	9.0	-19.5	23.3	-33.8	37.5	-48.1	4.5	18.7	-9.7	33.0	-24.0	47.3	-38.3	-4.5	9.7	-18.7	24.0	-33.0	38.3	-47.3	3.7	18.0	-10.5	32.3	-24.8	46.6	-39.0	-3.7	10.5	-18.0	24.8	-32.3	39.0	-46.6	3.0	17.2	-11.2	31.5	-25.5	45.8	-39.8	-3.0	11.2	-17.2	25.5	-31.5	39.8	-45.8	2.2	16.5	-12.0	30.8	-26.3	45.1	-40.6	-2.2	12.0	-16.5	26.3	-30.8	40.6	-45.1	1.5	15.7	-12.7	30.0	-27.0	44.3	-41.3	-1.5	12.7	-15.7	27.0	-30.0	41.3	-44.3	0.7	15.0	-13.5	29.3	-27.8	43.6	-42.1	-0.7	13.5	-15.0	27.8	-29.3	42.1	-43.6	0.0	14.2"),
        "Tuning (semi-tones)": -7,
        "Tuning (cents)": poly_to_list("!	2	6	0	7	0	6	7	7	5	6	6	2	1	2	4	3	5	7	1	4	0	3	1	3	4	3	6	6	2	3	1	2	6	2	1	1	3	4	5	6	6	2	5	1	3	0	1	6	1	5	6	4	1	1	0	6	5	5	4	6	5	0	7	4	1	3	4	3	4	4	7	5	6	5	5	5	3	7	1	0	1	5	3	3	6	1	4	1	3	2	6	5	5	4	2	4	2	1	7	5	4	1	5	3	1	4	2	1	5	1	4	4	0	6	0	1	4	7	4	0	6	0	0	6	3	6	1	1"),
        "Mod LFO freq(Hz)": 6.871,
        "Mod LFO pitch(c)": 0,
        "Vib LFO freq(Hz)": 3.440,
        "Vib LFO pitch(c)": -3,
    },
    "Instrument3": {
        "Attenuation (dB)": 18,
        "Pan": poly_to_list("!	6.7	21.0	-7.5	35.3	-21.8	49.6	-36.0	-6.7	7.5	-21.0	21.8	-35.3	36.0	-49.6	6.0	20.3	-8.2	34.5	-22.5	48.8	-36.8	-6.0	8.2	-20.3	22.5	-34.5	36.8	-48.8	5.2	19.5	-9.0	33.8	-23.3	48.1	-37.5	-5.2	9.0	-19.5	23.3	-33.8	37.5	-48.1	4.5	18.7	-9.7	33.0	-24.0	47.3	-38.3	-4.5	9.7	-18.7	24.0	-33.0	38.3	-47.3	3.7	18.0	-10.5	32.3	-24.8	46.6	-39.0	-3.7	10.5	-18.0	24.8	-32.3	39.0	-46.6	3.0	17.2	-11.2	31.5	-25.5	45.8	-39.8	-3.0	11.2	-17.2	25.5	-31.5	39.8	-45.8	2.2	16.5	-12.0	30.8	-26.3	45.1	-40.6	-2.2	12.0	-16.5	26.3	-30.8	40.6	-45.1	1.5	15.7	-12.7	30.0	-27.0	44.3	-41.3	-1.5	12.7	-15.7	27.0	-30.0	41.3	-44.3	0.7	15.0	-13.5	29.3	-27.8	43.6	-42.1	-0.7	13.5	-15.0	27.8	-29.3	42.1	-43.6	0.0	14.2"),
        "Tuning (semi-tones)": 0,
        "Tuning (cents)": poly_to_list("!	2	7	1	9	0	7	9	9	6	8	7	3	1	3	5	3	6	8	1	5	0	3	2	4	5	3	7	8	3	4	1	2	7	3	1	1	4	5	6	7	8	3	7	2	4	0	2	8	2	6	7	5	1	1	1	7	6	6	5	8	6	1	8	5	1	4	5	3	5	5	9	6	8	7	7	7	4	9	1	0	2	6	4	4	7	1	5	1	4	3	8	7	7	5	2	5	3	1	9	6	5	1	6	3	2	5	3	1	6	2	5	6	0	7	0	1	6	9	5	0	7	0	0	8	4	8	1	1"),
        "Mod LFO freq(Hz)": 6.871,
        "Mod LFO pitch(c)": 0,
        "Vib LFO freq(Hz)": 3.440,
        "Vib LFO pitch(c)": -2,
    },
    "Instrument4": {
        "Attenuation (dB)": 18,
        "Pan": poly_to_list("!	6.7	21.0	-7.5	35.3	-21.8	49.6	-36.0	-6.7	7.5	-21.0	21.8	-35.3	36.0	-49.6	6.0	20.3	-8.2	34.5	-22.5	48.8	-36.8	-6.0	8.2	-20.3	22.5	-34.5	36.8	-48.8	5.2	19.5	-9.0	33.8	-23.3	48.1	-37.5	-5.2	9.0	-19.5	23.3	-33.8	37.5	-48.1	4.5	18.7	-9.7	33.0	-24.0	47.3	-38.3	-4.5	9.7	-18.7	24.0	-33.0	38.3	-47.3	3.7	18.0	-10.5	32.3	-24.8	46.6	-39.0	-3.7	10.5	-18.0	24.8	-32.3	39.0	-46.6	3.0	17.2	-11.2	31.5	-25.5	45.8	-39.8	-3.0	11.2	-17.2	25.5	-31.5	39.8	-45.8	2.2	16.5	-12.0	30.8	-26.3	45.1	-40.6	-2.2	12.0	-16.5	26.3	-30.8	40.6	-45.1	1.5	15.7	-12.7	30.0	-27.0	44.3	-41.3	-1.5	12.7	-15.7	27.0	-30.0	41.3	-44.3	0.7	15.0	-13.5	29.3	-27.8	43.6	-42.1	-0.7	13.5	-15.0	27.8	-29.3	42.1	-43.6	0.0	14.2"),
        "Tuning (semi-tones)": 12,
        "Tuning (cents)": poly_to_list("!	2	8	1	10	0	8	9	10	7	9	8	3	1	3	6	4	7	9	2	5	0	4	2	5	5	4	8	9	4	4	1	2	8	3	1	1	4	5	7	8	9	3	8	2	4	0	2	9	2	7	8	5	2	1	1	8	7	7	5	9	7	1	9	5	1	4	6	4	5	5	10	7	9	7	7	8	4	10	1	0	2	7	4	4	8	1	6	1	4	4	8	7	7	6	3	6	3	1	10	7	6	1	7	4	2	5	3	2	7	2	5	6	1	8	1	1	6	10	6	0	8	0	1	8	5	9	1	1"),
        "Mod LFO freq(Hz)": 6.871,
        "Mod LFO pitch(c)": 0,
        "Vib LFO freq(Hz)": 3.440,
        "Vib LFO pitch(c)": -1,
    },
    "Instrument5": {
        "Attenuation (dB)": 18,
        "Pan": poly_to_list("!	6.7	21.0	-7.5	35.3	-21.8	49.6	-36.0	-6.7	7.5	-21.0	21.8	-35.3	36.0	-49.6	6.0	20.3	-8.2	34.5	-22.5	48.8	-36.8	-6.0	8.2	-20.3	22.5	-34.5	36.8	-48.8	5.2	19.5	-9.0	33.8	-23.3	48.1	-37.5	-5.2	9.0	-19.5	23.3	-33.8	37.5	-48.1	4.5	18.7	-9.7	33.0	-24.0	47.3	-38.3	-4.5	9.7	-18.7	24.0	-33.0	38.3	-47.3	3.7	18.0	-10.5	32.3	-24.8	46.6	-39.0	-3.7	10.5	-18.0	24.8	-32.3	39.0	-46.6	3.0	17.2	-11.2	31.5	-25.5	45.8	-39.8	-3.0	11.2	-17.2	25.5	-31.5	39.8	-45.8	2.2	16.5	-12.0	30.8	-26.3	45.1	-40.6	-2.2	12.0	-16.5	26.3	-30.8	40.6	-45.1	1.5	15.7	-12.7	30.0	-27.0	44.3	-41.3	-1.5	12.7	-15.7	27.0	-30.0	41.3	-44.3	0.7	15.0	-13.5	29.3	-27.8	43.6	-42.1	-0.7	13.5	-15.0	27.8	-29.3	42.1	-43.6	0.0	14.2"),
        "Tuning (semi-tones)": 19,
        "Tuning (cents)": poly_to_list("!	2	9	1	10	0	9	10	10	8	10	9	4	1	3	6	4	7	10	2	6	0	4	2	5	6	4	9	10	4	4	1	3	9	4	1	1	5	6	8	9	10	4	8	2	4	0	2	10	2	8	9	6	2	1	1	9	8	8	6	10	8	1	10	6	1	4	6	4	6	6	11	7	10	8	8	9	4	10	1	0	2	7	5	5	9	1	6	1	5	4	9	8	8	6	3	6	4	1	11	8	7	1	7	4	2	6	3	2	7	2	6	7	1	9	1	1	7	11	6	0	9	0	1	9	5	10	1	1"),
        "Mod LFO freq(Hz)": 6.871,
        "Mod LFO pitch(c)": 0,
        "Vib LFO freq(Hz)": 3.440,
        "Vib LFO pitch(c)": 0,
    },
    "Instrument6": {
        "Attenuation (dB)": 18,
        "Pan": poly_to_list("!	6.7	21.0	-7.5	35.3	-21.8	49.6	-36.0	-6.7	7.5	-21.0	21.8	-35.3	36.0	-49.6	6.0	20.3	-8.2	34.5	-22.5	48.8	-36.8	-6.0	8.2	-20.3	22.5	-34.5	36.8	-48.8	5.2	19.5	-9.0	33.8	-23.3	48.1	-37.5	-5.2	9.0	-19.5	23.3	-33.8	37.5	-48.1	4.5	18.7	-9.7	33.0	-24.0	47.3	-38.3	-4.5	9.7	-18.7	24.0	-33.0	38.3	-47.3	3.7	18.0	-10.5	32.3	-24.8	46.6	-39.0	-3.7	10.5	-18.0	24.8	-32.3	39.0	-46.6	3.0	17.2	-11.2	31.5	-25.5	45.8	-39.8	-3.0	11.2	-17.2	25.5	-31.5	39.8	-45.8	2.2	16.5	-12.0	30.8	-26.3	45.1	-40.6	-2.2	12.0	-16.5	26.3	-30.8	40.6	-45.1	1.5	15.7	-12.7	30.0	-27.0	44.3	-41.3	-1.5	12.7	-15.7	27.0	-30.0	41.3	-44.3	0.7	15.0	-13.5	29.3	-27.8	43.6	-42.1	-0.7	13.5	-15.0	27.8	-29.3	42.1	-43.6	0.0	14.2"),
        "Tuning (semi-tones)": 24,
        "Tuning (cents)": poly_to_list("!	3	10	1	11	0	10	11	11	8	11	10	4	1	4	7	5	8	11	2	6	1	5	2	6	6	5	10	11	4	5	1	3	10	4	1	1	5	6	9	9	11	4	9	2	5	0	2	10	2	8	10	6	2	1	1	10	9	9	6	11	9	1	11	6	1	5	7	5	6	6	12	8	11	9	9	9	5	11	1	0	3	8	5	5	10	2	7	2	5	4	10	9	9	7	3	7	4	1	12	9	7	2	8	5	2	6	4	2	8	2	6	7	1	9	1	1	8	12	7	0	10	0	1	10	6	11	1	1"),
        "Mod LFO freq(Hz)": 6.871,
        "Mod LFO pitch(c)": 0,
        "Vib LFO freq(Hz)": 3.440,
        "Vib LFO pitch(c)": 1,
    },
    "Instrument7": {
        "Attenuation (dB)": 18,
        "Pan": poly_to_list("!	6.7	21.0	-7.5	35.3	-21.8	49.6	-36.0	-6.7	7.5	-21.0	21.8	-35.3	36.0	-49.6	6.0	20.3	-8.2	34.5	-22.5	48.8	-36.8	-6.0	8.2	-20.3	22.5	-34.5	36.8	-48.8	5.2	19.5	-9.0	33.8	-23.3	48.1	-37.5	-5.2	9.0	-19.5	23.3	-33.8	37.5	-48.1	4.5	18.7	-9.7	33.0	-24.0	47.3	-38.3	-4.5	9.7	-18.7	24.0	-33.0	38.3	-47.3	3.7	18.0	-10.5	32.3	-24.8	46.6	-39.0	-3.7	10.5	-18.0	24.8	-32.3	39.0	-46.6	3.0	17.2	-11.2	31.5	-25.5	45.8	-39.8	-3.0	11.2	-17.2	25.5	-31.5	39.8	-45.8	2.2	16.5	-12.0	30.8	-26.3	45.1	-40.6	-2.2	12.0	-16.5	26.3	-30.8	40.6	-45.1	1.5	15.7	-12.7	30.0	-27.0	44.3	-41.3	-1.5	12.7	-15.7	27.0	-30.0	41.3	-44.3	0.7	15.0	-13.5	29.3	-27.8	43.6	-42.1	-0.7	13.5	-15.0	27.8	-29.3	42.1	-43.6	0.0	14.2"),
        "Tuning (semi-tones)": 28,
        "Tuning (cents)": poly_to_list("!	3	10	1	12	0	11	12	12	9	12	11	4	1	4	7	5	9	12	2	7	1	5	2	6	7	5	11	12	5	5	1	3	11	4	1	1	5	7	9	10	12	4	10	2	5	0	2	11	3	9	10	7	2	1	1	11	9	9	7	12	9	1	12	7	1	5	8	5	7	7	13	9	12	10	10	10	5	12	1	0	3	9	5	6	10	2	8	2	6	5	11	10	9	7	3	8	4	2	13	9	8	2	8	5	3	7	4	2	9	2	7	8	1	10	1	1	8	13	7	0	11	0	1	11	6	12	1	1"),
        "Mod LFO freq(Hz)": 6.871,
        "Mod LFO pitch(c)": 0,
        "Vib LFO freq(Hz)": 3.440,
        "Vib LFO pitch(c)": 2,
    },
    "Instrument8": {
        "Attenuation (dB)": 18,
        "Pan": poly_to_list("!	6.7	21.0	-7.5	35.3	-21.8	49.6	-36.0	-6.7	7.5	-21.0	21.8	-35.3	36.0	-49.6	6.0	20.3	-8.2	34.5	-22.5	48.8	-36.8	-6.0	8.2	-20.3	22.5	-34.5	36.8	-48.8	5.2	19.5	-9.0	33.8	-23.3	48.1	-37.5	-5.2	9.0	-19.5	23.3	-33.8	37.5	-48.1	4.5	18.7	-9.7	33.0	-24.0	47.3	-38.3	-4.5	9.7	-18.7	24.0	-33.0	38.3	-47.3	3.7	18.0	-10.5	32.3	-24.8	46.6	-39.0	-3.7	10.5	-18.0	24.8	-32.3	39.0	-46.6	3.0	17.2	-11.2	31.5	-25.5	45.8	-39.8	-3.0	11.2	-17.2	25.5	-31.5	39.8	-45.8	2.2	16.5	-12.0	30.8	-26.3	45.1	-40.6	-2.2	12.0	-16.5	26.3	-30.8	40.6	-45.1	1.5	15.7	-12.7	30.0	-27.0	44.3	-41.3	-1.5	12.7	-15.7	27.0	-30.0	41.3	-44.3	0.7	15.0	-13.5	29.3	-27.8	43.6	-42.1	-0.7	13.5	-15.0	27.8	-29.3	42.1	-43.6	0.0	14.2"),
        "Tuning (semi-tones)": 31,
        "Tuning (cents)": poly_to_list("!	3	11	1	13	0	12	13	13	10	13	11	5	2	4	8	5	9	13	2	7	1	5	3	7	7	5	12	13	5	6	1	3	12	5	2	1	6	7	10	11	13	5	11	2	6	0	3	12	3	10	11	7	2	1	1	12	10	10	8	13	10	1	13	7	1	6	8	5	7	7	14	9	13	10	10	11	5	13	1	0	3	9	6	6	11	2	8	2	6	5	12	10	10	8	4	8	5	2	14	10	8	2	9	5	3	7	4	2	9	2	8	9	1	11	1	1	9	14	8	0	12	0	1	12	7	13	1	1"),
        "Mod LFO freq(Hz)": 6.871,
        "Mod LFO pitch(c)": 0,
        "Vib LFO freq(Hz)": 3.440,
        "Vib LFO pitch(c)": 3,
    },
    "Instrument9": {
        "Attenuation (dB)": 18,
        "Pan": poly_to_list("!	6.7	21.0	-7.5	35.3	-21.8	49.6	-36.0	-6.7	7.5	-21.0	21.8	-35.3	36.0	-49.6	6.0	20.3	-8.2	34.5	-22.5	48.8	-36.8	-6.0	8.2	-20.3	22.5	-34.5	36.8	-48.8	5.2	19.5	-9.0	33.8	-23.3	48.1	-37.5	-5.2	9.0	-19.5	23.3	-33.8	37.5	-48.1	4.5	18.7	-9.7	33.0	-24.0	47.3	-38.3	-4.5	9.7	-18.7	24.0	-33.0	38.3	-47.3	3.7	18.0	-10.5	32.3	-24.8	46.6	-39.0	-3.7	10.5	-18.0	24.8	-32.3	39.0	-46.6	3.0	17.2	-11.2	31.5	-25.5	45.8	-39.8	-3.0	11.2	-17.2	25.5	-31.5	39.8	-45.8	2.2	16.5	-12.0	30.8	-26.3	45.1	-40.6	-2.2	12.0	-16.5	26.3	-30.8	40.6	-45.1	1.5	15.7	-12.7	30.0	-27.0	44.3	-41.3	-1.5	12.7	-15.7	27.0	-30.0	41.3	-44.3	0.7	15.0	-13.5	29.3	-27.8	43.6	-42.1	-0.7	13.5	-15.0	27.8	-29.3	42.1	-43.6	0.0	14.2"),
        "Tuning (semi-tones)": 36,
        "Tuning (cents)": poly_to_list("!	3	12	1	14	0	12	14	14	11	13	12	5	2	5	8	6	10	14	2	8	1	6	3	7	8	6	12	14	5	6	1	4	12	5	2	1	6	8	11	12	14	5	12	3	6	0	3	13	3	10	12	8	2	1	1	12	11	11	8	13	11	1	14	8	1	6	9	6	8	8	14	10	14	11	11	12	6	14	2	0	3	10	6	6	12	2	9	2	7	5	13	11	11	8	4	9	5	2	15	11	9	2	10	6	3	8	5	2	10	3	8	9	1	12	1	1	10	15	8	0	12	0	1	13	7	14	1	1"),
        "Mod LFO freq(Hz)": 6.871,
        "Mod LFO pitch(c)": 0,
        "Vib LFO freq(Hz)": 3.440,
        "Vib LFO pitch(c)": 4,
    },
}

stops = {
    "Great": {
        "Trumpet8'":{
            "ID":"1",
            "Instrument3":10.0,
            "Instrument4":5.0,
            "Instrument5":5.0,
            "Instrument6":5.0,
            "Instrument7":5.0,
            "Instrument8":10.0,
            "Instrument9":35.0,
            "Noise breath": 0.0,
            "Chiff": 0.0,
            "Notes":[]
        },
        "Mixture5":{
            "ID":"2",
            "Instrument4":0.0,
            "Instrument5":5.0,
            "Instrument6": 5.0,
            "Instrument7": 15.0,
            "Instrument8": 20.0,
            "Instrument9": 10.0,
            "Noise breath": 0.0,
            "Chiff": 0.0,
            "Notes":[]
        },
        "Cornet2&4'":{
            "ID":"3",
            "Instrument4":5.0,
            "Instrument6":0.0,
            "Instrument7":5.0,
            "Instrument8":10.0,
            "Instrument9":25.0,
            "Noise breath": 0.0,
            "Chiff": 0.0,
            "Notes":[]
        },
        "Raushquinte2_2-3":{
            "ID":"4",
            "Instrument5":-5.0,
            "Instrument6":10.0,
            "Instrument9":10.0,
            "Noise breath": 0.0,
            "Chiff": 0.0,
            "Notes":[]
        },
        "Piccolo2'":{
            "ID":"5",
            "Instrument6":0.0,
            "Instrument9":15.0,
            "Noise breath": 0.0,
            "Chiff": 0.0,
            "Notes":[]
        },
        "Octave4'":{
            "ID":"6",
            "Instrument4":-5.0,
            "Instrument6":10.0,
            "Instrument8":30.0,
            "Noise breath": 0.0,
            "Chiff": 0.0,
            "Notes":[]
        },
        "HohlFlute4'":{
            "ID":"7",
            "Instrument4":0.0,
            "Instrument6":5.0,
            "Noise breath": 0.0,
            "Chiff": 0.0,
            "Notes":[]
        },
        "HarmonicFlute8'":{
            "ID":"8",
            "Instrument3":5.0,
            "Instrument4": 0.0,
            "Instrument5": 10.0,
            "Instrument6": 20.0,
            "Noise breath": 0.0,
            "Chiff": 0.0,
            "Notes":[]
        },
        "Rohrflote8'": {
            "ID": "9",
            "Instrument3": 5.0,
            "Instrument4": 25.0,
            "Instrument5": 20.0,
            "Instrument7": 30.0,
            "Noise breath": 0.0,
            "Chiff": 0.0,
            "Notes":[]
        },
        "Salicional8'": {
            "ID": "A",
            "Instrument3": 30.0,
            "Instrument4": 20.0,
            "Instrument5": 15.0,
            "Instrument6": 20.0,
            "Instrument7": 25.0,
            "Instrument8": 30.0,
            "Instrument9": 35.0,
            "Noise breath": 0.0,
            "Chiff": 0.0,
            "Notes":[]
        },
        "Gamba8'": {
            "ID": "B",
            "Instrument3":25.0,
            "Instrument4": 20.0,
            "Instrument5": 5.0,
            "Instrument6": 20.0,
            "Instrument7": 20.0,
            "Instrument8": 25.0,
            "Instrument9": 35.0,
            "Noise breath": 0.0,
            "Chiff": 0.0,
            "Notes":[]
        },
        "Prinzipal8'": {
            "ID": "C",
            "Instrument3": 0.0,
            "Instrument4": 0.0,
            "Instrument5": 5.0,
            "Instrument6": 15.0,
            "Instrument7": 15.0,
            "Instrument8": 25.0,
            "Noise breath": 0.0,
            "Chiff": 0.0,
            "Notes":[]
        },
        "Bourdon16'": {
            "ID": "D",
            "Instrument1": 0.0,
            "Instrument2": 35.0,
            "Noise Breath": 0.0,
            "Chiff": 0.0,
            "Notes":[]
        },
        "Prinzipal16'": {
            "ID": "E",
            "Instrument1": 5.0,
            "Instrument2": 35.0,
            "Instrument3": 5.0,
            "Instrument4": 5.0,
            "Instrument5": 15.0,
            "Instrument6": 20.0,
            "Instrument7": 25.0,
            "Instrument8": 30.0,
            "Noise Breath": 0.0,
            "Chiff": 0.0,
            "Notes":[]
        },
    },
    "Swell":{
        "Lieblgedekt 16'":{
            "ID": "1",
            "Instrument1": -10.0,
            "Noise breath": 0.0,
            "Chiff": 0.0,
            "Notes": []
        },
        "GeigenPrinzipl16":{
            "ID": "2",
            "Noise breath": 0.0,
            "Chiff": 0.0,
            "Notes": [],
            "Instrument2": 35.0,
            "Instrument3": 5.0,
            "Instrument4": 5.0,
            "Instrument5": 15.0,
            "Instrument6": 20.0,
            "Instrument7": 25.0,
            "Instrument8": 30.0,
        },
        "Gedekt 8'":{
            "ID": "3",
            "Noise breath": 0.0,
            "Chiff": 0.0,
            "Notes": [],
            "Instrument3": -10.0,
            "Instrument4": 10.0,
        },
        "Concert Flute 8":{
            "ID": "4",
            "Noise breath": 0.0,
            "Chiff": 0.0,
            "Notes": [],
            "Instrument3": -10.0,
            "Instrument5": 5.0,
        },
        "Gemshorn 8'":{
            "ID": "5",
            "Noise breath": 0.0,
            "Chiff": 0.0,
            "Notes": [],
            "Instrument3": 15.0,
            "Instrument4": 0.0,
            "Instrument5": 15.0,
            "Instrument6": 25.0,
            "Instrument7": 20.0,
            "Instrument8": 25.0,
            "Instrument9": 30.0,
        },
        "Chalumeau 8'":{
            "ID": "6",
            "Noise breath": 0.0,
            "Chiff": 0.0,
            "Notes": [],
            "Instrument3": 10.0,
            "Instrument4": 25.0,
            "Instrument5": 10.0,
            "Instrument6": 20.0,
            "Instrument7": 15.0,
            "Instrument8": 20.0,
            "Instrument9": 25.0,
        },
        "Aeoline 8'":{
            "ID": "7",
            "Noise breath": 0.0,
            "Chiff": 0.0,
            "Notes": [],
            "Instrument3": 25.0,
            "Instrument4": 15.0,
            "Instrument5": 20.0,
            "Instrument6": 25.0,
            "Instrument7": 25.0,
            "Instrument9": 25.0,
        },
        "VoixCeleste 8'":{
            "ID": "8",
            "Noise breath": 0.0,
            "Chiff": 0.0,
            "Notes": [],
            "Instrument3": 30.0,
            "Instrument4": 15.0,
            "Instrument5": 10.0,
            "Instrument6": 15.0,
            "Instrument7": 30.0,
            "Instrument8": 25.0,
            "Instrument9": 25.0,
        },
        "Fugara 4'":{
            "ID": "9",
            "Noise breath": 0.0,
            "Chiff": 0.0,
            "Notes": [],
            "Instrument3": None,
            "Instrument4": 5.0,
            "Instrument5": None,
            "Instrument6": 0.0,
            "Instrument7": None,
            "Instrument8": 5.0,
            "Instrument9": 10.0,
        },
        "Travesflute 4'":{
            "ID": "A",
            "Noise breath": 0.0,
            "Chiff": 0.0,
            "Notes": [],
            "Instrument3": None,
            "Instrument4": -5.0,
            "Instrument5": 15.0,
            "Instrument6": None,
            "Instrument7": None,
            "Instrument8": None,
            "Instrument9": None,
        },
        "Progressio 2&4'":{
            "ID": "B",
            "Noise breath": 0.0,
            "Chiff": 0.0,
            "Notes": [],
            "Instrument2": 0.0,
            "Instrument3": None,
            "Instrument4": 0.0,
            "Instrument5": 0.0,
            "Instrument6": 0.0,
            "Instrument7": None,
            "Instrument8": 0.0,
            "Instrument9": None,
        },
    },
    "Pedal": {
        "Posaune16'":{
            "ID": "1",
            "Noise breath": 0.0,
            "Chiff": 0.0,
            "Notes": [],
            "Global": -5.0,
            "Instrument1": -5.0,
            "Instrument2": -10.0,
            "Instrument3": -10.0,
            "Instrument4": -5.0,
            "Instrument5": 25.0,
            "Instrument6": 10.0,
            "Instrument7": 15.0,
            "Instrument8": 25.0,
            "Instrument9": None,
        },        
        "PrinzipalBass16":{
            "ID": "2",
            "Noise breath": 0.0,
            "Chiff": 0.0,
            "Notes": [],
            "Global": -5.0,
            "Instrument1": 0.0,
            "Instrument2": 30.0,
            "Instrument3": 0.0,
            "Instrument4": 0.0,
            "Instrument5": 10.0,
            "Instrument6": 15.0,
            "Instrument7": 20.0,
            "Instrument8": 25.0,
            "Instrument9": None,
        },        
        "BassViolin16'":{
            "ID": "3",
            "Noise breath": 0.0,
            "Chiff": 0.0,
            "Notes": [],
            "Global": -5.0,
            "Instrument1": -5.0,
            "Instrument2": 10.0,
            "Instrument3": 5.0,
            "Instrument4": 10.0,
            "Instrument5": 15.0,
            "Instrument6": 25.0,
            "Instrument7": None,
            "Instrument8": None,
            "Instrument9": None,
        },
        "SubBass16'":{
            "ID": "4",
            "Noise breath": 0.0,
            "Chiff": 0.0,
            "Notes": [],
            "Global": -5.0,
            "Instrument1": -5.0,
            "Instrument2": -5.0,
            "Instrument3": None,
            "Instrument4": None,
            "Instrument5": None,
            "Instrument6": None,
            "Instrument7": None,
            "Instrument8": None,
            "Instrument9": None,
        },
        "EchoBass16'":{
            "ID": "5",
            "Noise breath": 0.0,
            "Chiff": 0.0,
            "Notes": [],
            "Global": -5.0,
            "Instrument1": 5.0,
            "Instrument2": None,
            "Instrument3": 5.0,
            "Instrument4": 15.0,
            "Instrument5": 15.0,
            "Instrument6": None,
            "Instrument7": None,
            "Instrument8": None,
            "Instrument9": None,
        },
        "Quintbass10_2-3":{
            "ID": "6",
            "Noise breath": 0.0,
            "Chiff": 0.0,
            "Notes": [],
            "Global": -5.0,
            "Instrument1": None,
            "Instrument2": -10.0,
            "Instrument3": None,
            "Instrument4": None,
            "Instrument5": None,
            "Instrument6": None,
            "Instrument7": None,
            "Instrument8": None,
            "Instrument9": None,
        },
        "OctaveBass8'":{
            "ID": "7",
            "Noise breath": 0.0,
            "Chiff": 0.0,
            "Notes": [],
            "Global": -5.0,
            "Instrument1": None,
            "Instrument2": None,
            "Instrument3": -10.0,
            "Instrument4": 5.0,
            "Instrument5": 25.0,
            "Instrument6": None,
            "Instrument7": None,
            "Instrument8": None,
            "Instrument9": None,
        },
        "Violoncello8'":{
            "ID": "8",
            "Noise breath": 0.0,
            "Chiff": 0.0,
            "Notes": [],
            "Global": -5.0,
            "Instrument1": None,
            "Instrument2": None,
            "Instrument3": 20.0,
            "Instrument4": 0.0,
            "Instrument5": 5.0,
            "Instrument6": 0.0,
            "Instrument7": 10.0,
            "Instrument8": 15.0,
            "Instrument9": 25.0,
        },
        "BassFlute8'":{
            "ID": "9",
            "Noise breath": 0.0,
            "Chiff": 0.0,
            "Notes": [],
            "Global": -5.0,
            "Instrument1": None,
            "Instrument2": None,
            "Instrument3": -5.0,
            "Instrument4": 0.0,
            "Instrument5": 15.0,
            "Instrument6": None,
            "Instrument7": None,
            "Instrument8": None,
            "Instrument9": None,
        },
        "OctaveBass4'":{
            "ID": "A",
            "Noise breath": 0.0,
            "Chiff": 0.0,
            "Notes": [],
            "Global": -5.0,
            "Instrument1": None,
            "Instrument2": None,
            "Instrument3": None,
            "Instrument4": -10.0,
            "Instrument5": None,
            "Instrument6": 5.0,
            "Instrument7": None,
            "Instrument8": 25.0,
            "Instrument9": None,
        },
    },
}

stops_sheet = """
+--+----------------------+--+-----------------------+--+---------------------+
| #| [MI] (Great)         | #|  [MII] (Swell)        | #| [P] (Pedal)         |
|--+----------------------+--+-----------------------+--+---------------------+
|01| G 1-Trumpet8'        |01|  S 1-Lieblgedekt 16'  |01| P 1-Posaune16'      |
|02| G 2-Mixture5         |02|  S 2-GeigenPrinzipl16 |02| P 2-PrinzipalBass16 |
|03| G 3-Cornet2&4'       |03|  S 3-Gedekt 8'        |03| P 3-BassViolin16'   |
|04| G 4-Raushquinte2_2/3 |04|  S 4-Concert Flute 8  |04| P 4-SubBass16'      |
|05| G 5-Piccolo2'        |05|  S 5-Gemshorn 8'      |05| P 5-EchoBass16'     |
|06| G 6-Octave4'         |06|  S 6-Chalumeau 8'     |06| P 6-Quintbass10_2/3 |
|07| G 7-HohlFlute4'      |07|  S 7-Aeoline 8'       |07| P 7-OctaveBass8'    |
|08| G 8-HarmonicFlute8'  |08|  S 8-VoixCeleste 8'   |08| P 8-Violoncello8'   |
|09| G 9-Rohrflote8'      |09|  S 9-Fugara 4'        |09| P 9-BassFlute8'     |
|10| G A-Salicional8'     |10|  S A-Travesflute 4'   |10| P A-OctaveBass4'    |
|11| G B-Gamba8'          |11|  S B-Progressio 2&4'  +------------------------+
|12| G C-Prinzipal8'      +--------------------------+
|13| G D-Bourdon16'       |
|14| G E-Prinzipal16'     |
+--+----------------------+
"""
